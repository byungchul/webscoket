# Release v0.2
## 배차기능 이름변경
- function 이름 "start" -> "go" 로 변경
- function 이름 "complete" -> "arrived" 로 변경 
## 배차 프로토콜 추가 
- function cancel_call 추가: 타시오앱에서 배차 취소할 경우
## 배차 필드 추가 및 변경 
- 타시오앱에서 vehicle_id는 군산의 경우 4,5이므로 둘중 아무거나 채워서 전달 요청 
- 안전요원앱에서 전달받은 vehicle_id로 다시 채워 타시오앱과 관제앱으로 전달 
- 안전요원앱에서 서버로 "go"전달시 "current_station_eta", "target_station_eta" 채워서 타시오앱으로 전달.
---
# Websocket Server와 Client간 통신 방법 

## 접속 URL 
```
ws://222.114.39.8:11411
```

## Websocket Server와 Client간 메시지 정의 
```text
{
    version: 1
    when:  UTC 시간
    where: 호출한 앱의 위치
    who:   호출한 앱의 고유 이름 * 단 서버에서 보낼때는 직전 호출자 명시 
    what:  REQ, RESP, EVENT, ERROR, PING중 하나
    how:   {} what에 따른 key:value
}
```
![Alt text](./images/websocket_server_apps_connection_flow.png "WebSocket Server와 Application간 연결 흐름")
### what: EVENT 정의
---
* passenger EVENT

```text
{
    what: EVENT
    how: {
        type: passenger(string)
        vehicle_id:  vehicle의 ID(number)
        current_passenger: 현재탑승 인원(number)
        accumulated_passenger: 오늘 누적된 탑승인원(number)
    }
}
```
* passenger 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "",
    "who": "safety_id",
    "what": "EVENT",
    "how": {
        "type": "passenger",
        "vehicle_id": 6,
        "current_passenger": 1,
        "accumulated_passenger": 17
    }
}
```
* passenger RESP
```text
{
    what: RESP
    how: {
        type: passenger(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        current_passenger: 현재탑승 인원(number)
        accumulated_passenger: 오늘 누적된 탑승인원(number)
    }
}
```

* passenger 예시 

```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "RESP",
    "how": {
        "type": "passenger",
        "vehicle_id": 6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "current_passenger": 1,
        "accumulated_passenger": 17
    }
}
```
---
* power EVENT

```text
{
    what: EVENT
    how: {
        type: power(string)
        vehicle_id:  vehicle의 ID(number)
        value: 차량 시동시 또는 차량 운행 시작시(string:on/off)
    }
}
```
* power 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "",
    "who": "safety_id",
    "what": "EVENT",
    "how": {
        "type": "power",
        "vehicle_id": 6,
        "value": "on"
    }
}
```
* power RESP

```text
{
    what: RESP
    how: {
        type: passenger(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        value: 차량 시동시 또는 차량 운행 시작시(string:on/off)
    }
}
```
* power 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "RESP",
    "how": {
        "type": "power",
        "vehicle_id":6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "value": "on"
    }
}
```
---
* parking EVENT

```text
{
    what: EVENT
    how: {
        type: parking(string)
        vehicle_id:  vehicle의 ID(number)
        value: 차량 주차 여부(string:true/false)
    }
}
```

* parking RESP

```text
{
    what: RESP
    how: {
        type: parking(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        value: 차량 주차 여부(string:true/false)
    }
}
```

* parking 예시 

```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "RESP",
    "how": {
        "type": "parking",
        "vehicle_id": 6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "value": "trur"
    }
}
```
---
* drive EVENT

```text
{
    what: EVENT
    how: {
        type: drive(string)
        vehicle_id:  vehicle의 ID(number)
        value: 차량 운행 모드(string:auto/normal)
    }
}
```

* drive RESP

```text
{
    what: RESP
    how: {
        type: drive(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        value: 차량 운행 모드(string:auto/normal)
    }
}
```

* drive 예시 

```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "RESP",
    "how": {
        "type": "drive",
        "vehicle_id": 6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "value": "auto"
    }
}
```
---
* door EVENT

```text
{
    what: EVENT
    how: {
        type: door(string)
        vehicle_id:  vehicle의 ID(number)
        value: 차량 문열림 여부(string:true/false)
    }
}
```

* door RESP

```text
{
    what: RESP
    how: {
        type: door(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        value: 차량 문열림 여부(string:true/false)
    }
}
```

* door 예시 

```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "RESP",
    "how": {
        "type": "door",
        "vehicle_id": 6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "value": "true"
    }
}
```
---
* message EVENT

```text
{
    what: EVENT
    how: {
        type: message(string)
        vehicle_id:  vehicle의 ID(number)
        value: 전달할 메시지(string)
    }
}
```

* message RESP

```text
{
    what: RESP
    how: {
        type: message(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        value: 전달할 메시지(string)
    }
}
```
* message 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "RESP",
    "how": {
        "type": "message",
        "vehicle_id": 6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "value": "안녕하세요"
    }
}
```
---
* station EVENT

```text
{
    what: EVENT
    how: {
        type: station(string)
        vehicle_id:  vehicle의 ID(number)
        value: station_id(number)
    }
}
```

* station RESP

```text
{
    what: RESP
    how: {
        type: station(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        value: station_id(number)
    }
}
```
* station 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "",
    "who": "safe_id1394",
    "what": "EVENT",
    "how": {
        "type": "station",
        "vehicle_id": 6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "value": 2
    }
}
```
![Alt text](./images/ondemand.jpeg "onDemand 서비스를 위한 서버/클라이언트간 연결 흐름")
---
* ondemand CALL EVENT(배차 요청)
* tasio -> server
```text
{
    where: ''  # not mandatory
    who: tasio_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)  # 그지역의 유효한 차량번호
        function: call(string)
        current_station_id: id(number)
        target_station_id: id(number)
        passenger: 승객수(number)    
    }
}
```
* ondemand RESP
```text
{
    where: sejong_datahub
    who: server_id
    what: RESP
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)  # 최적의 차량을 선정해 응답으로 safe에 보내줌.
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: call(string)
        current_station_id: id(number)
        target_station_id: id(number)
        passenger: 승객수(number)  
    }
}
```
* ondemand 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "RESP",
    "how": {
        "type": "ondemand",
        "vehicle_id": 6,
        "vehicle_mid": "SCN999",
        "site_id": 2,
        "function": "call",
        "current_station_id": 1,
        "target_station_id": 3,
        "passenger": 1,
    }
}
```
---
* ondemand CALL EVENT(배차 취소)
* tasio -> server
```text
{
    where: ''  # not mandatory
    who: tasio_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)  # 그지역의 유효한 차량번호
        function: cancel_call(string)
        current_station_id: id(number)
        target_station_id: id(number)
        passenger: 승객수(number)    
    }
}
```
---
* ondemand CALL EVENT(배차 요청)
* server -> safety
```text
{
    where: sejong_datahub
    who: server_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)  # 그지역의 유요한 차량번호
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: call(string)
        current_station_id: id(number)
        target_station_id: id(number)
        passenger: 승객수(number)
    }
}
```
---
* ondemand GO EVENT(배차 확인)
* safety -> server
```text
{
    where: ''  # not mandatory
    who: safety_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)  # 안전요원앱에서 받은 차량번호로 변경하여 전달(to tasio)
        function: go(string)
    }
}
```
* ondemand RESP
```text
{
    where: sejong_datahub
    who: server_id
    what: RESP
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: go(string)
    }
}
```
---
* ondemand GO EVENT(배차 확인)
* server -> tasio (go)
```text
{
    where: sejong_datahub
    who: server_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)  # 안전요원앱에서 받은 차량번호 
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: go(string)
        current_station_eta: 현재 정류장까지 오는데 걸리는시간(number/min)
        target_station_eta: 목적지 정류장까지 이동하는데 걸리는시간(number/min)
    }
}
```
---
* ondemand GO EVENT(배차 확인)
* server -> web (go)
```text
{
    where: sejong_datahub
    who: server_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)  # 안전요원앱에서 받은 차량번호 
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: go(string)
        current_station_eta: 현재 정류장까지 오는데 걸리는시간(number/min)
        target_station_eta: 목적지 정류장까지 이동하는데 걸리는시간(number/min)
    }
}
```
---
* ondemand ARRIVED EVENT(배차 완료)
* safety -> server (arrived)
```text
{
    where: ''  # not mandatory
    who: safety_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)
        function: arrived(string)
    }
}
```
* ondemand RESP
```text
{
    where: sejong_datahub
    who: server_id
    what: RESP
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: arrived(string)
    }
}
```
---
* ondemand ARRIVED EVENT(배차 완료)
* server -> tasio (arrived)
```text
{
    where: sejong_datahub
    who:[safety_id]
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: arrived(string)
    }
}
```
---
* ondemand ARRIVED EVENT(배차 완료)
* server -> web (arrived)
```text
{
    where: sejong_datahub
    who: server_id
    what: EVENT
    how: {
        type: ondemand(string)
        vehicle_id:  vehicle의 ID(number)
        vehicle_mid: vheicle의 관리 ID(string)
        site_id: vehicle에 소속된 site의 ID(number)
        function: arrived(string)
    }
}
```
---
* identity EVENT
* server -> connected client
```text
{
    where: sejong_datahub
    who: server_id
    what: EVENT
    how: {
        type: identity(string)
        value: address(string)
    }
}
```
* identity 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "sejong_datahub",
    "who": "server_id",
    "what": "EVENT",
    "how": {
        "type": "identity",
        "value": "211.134.131.2"
    }
}
```
---
* identity EVENT
* connected client -> server
```text
{
    where: ''  # not mandatory
    who: "connected client"
    what: EVENT
    how: {
        type: identity(string)
        value: tasio_id(string)
    }
}
```
* identity 예시 
```json
{
    "version": 1,
    "when":  1592372883.060738,
    "where": "",
    "who": "tasio_3949",
    "what": "EVENT",
    "how": {
        "type": "identity",
        "value": "tasio_3949"
    }
}
```
### what: PING 정의
### 연결된 Client에 주기적인 PING(Alive) 전송
---
* PING의 예시

```json
{
    "version": 1,
    "when": 1592372883.060738,
    "where": "sejong_datahub",
    "what": "PING",
    "who": "server_id"
    "how": {
        "ipaddr": "211.131.23.1"
    }
}
```
---
## TODO
### 배차 시퀀스 정의
* 시퀀스 그림 추가
* 포맷 정의 
